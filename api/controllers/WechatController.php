<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-19
 * Time: 上午11:24
 */

namespace api\controllers;


use api\ext\UploadFile;
use api\models\Wechat;
use corephp\App;
use corephp\db\Model;
use corephp\web\Request;

class WechatController extends Base
{
    public function bind()
    {
        if (Request::isPost()) {
            $post = Request::post();
            $wechat = new Wechat();
            $status = $wechat->data([
                'appid'      => $post['appid'],
                'appsecrect' => $post['appsecrect']
            ])->where(['id' => 1])->update();
            return json_encode([
                'status' => 1
            ]);
        }

        $wechat = (new Wechat())->field('*')->get();
        return json_encode($wechat);


    }

    public function qrcode()
    {
        $model =new Wechat();
        if(Request::isPost()){
            $upload = new UploadFile();// 实例化上传类
            $upload->maxSize  = 3145728 ;// 设置附件上传大小
            $upload->allowExts  = array('jpg', 'png', 'jpeg');// 设置附件上传类型
            $upload->savePath =  'upload/food/';// 设置附件上传目录
            if(!$upload->upload()) {// 上传错误提示错误信息
                $msg = $upload->getErrorMsg();
                $result['status']=0;
                $result['msg'] = $msg;
            }else{// 上传成功
                $imginfo=$upload->getUploadFileInfo();
                $result['status']=1;
                $result['msg'] = $imginfo[0];
                $model->data([
                    'qrcode'=>$imginfo[0]['savepath'].$imginfo[0]['savename']
                ])->where(['id'=>1])->update();
            }
            return json_encode($result);
        }
        $wechat = $model->get();
        return json_encode($wechat);

    }
}