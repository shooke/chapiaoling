<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-19
 * Time: 上午11:24
 */

namespace api\controllers;

use api\models\SiteSetting;
use api\models\Wechat;
use corephp\App;
use corephp\db\Model;
use corephp\web\Request;
use corephp\web\Controller;

class SiteController extends Base
{
    public function setting()
    {
        $model = new SiteSetting();
        if (Request::isPost()) {
            $post = Request::post();

            $status = $model->data([
                'title'       => $post['title'],
                'keyword'     => $post['keyword'],
                'description' => $post['description'],
                'ipc'         => $post['ipc'],
                'script'      => $post['script'],
            ])->where(['id' => 1])->update();
            return json_encode([
                'status' => 1
            ]);
        }

        $setting = $model->field('*')->get();
        return json_encode($setting);
    }

}