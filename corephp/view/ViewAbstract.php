<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-12
 * Time: 上午10:40
 */

namespace corephp\view;


abstract class ViewAbstract
{
    protected $contentFile;
    protected $layoutFile;

    /**
     * 内容模板编译
     * @return mixed
     */
    abstract public function contentCompile();

    /**
     * 布局模板编译
     * @return mixed
     */
    abstract public function layoutCompile();

    public function layout($layoutFile)
    {
        $this->layoutFile = $layoutFile;
    }
    public function render($contentFile,$vars)
    {
        $this->contentFile = $contentFile;
        $content = $this->contentCompile();
        $layout = $this->layoutCompile();
        
    }
}