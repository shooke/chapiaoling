<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/21
 * Time: 18:34
 */

namespace api\controllers;


use api\ext\Category;
use api\models\WechatMenu;
use corephp\web\Request;

class WechatMenuController extends Base
{
    public function menus()
    {
        $wm = new WechatMenu();
        $menu = $wm->orderBy(['sort'=>'ASC'])->select();
        $cat=new Category(array('id','pid','name','cname'));
        $menu=$cat->getTree($menu);//获取分类数据树结构
        $topmenu = [
            ['id'=>0,'pid'=>0,'name'=>'顶级菜单']
        ];
        foreach ($menu as $val){
            if($val['pid']==0){
                $topmenu[] = $val;
            }
        }
        return json_encode([
            'menu'=>$menu,
            'topmenu'=>$topmenu
        ]);
    }
    public function edit()
    {
        $post = Request::post();
        $wm = new WechatMenu();
        $wm->where(['id'=>$post['id']])->data([
            'pid'=>$post['pid'],
            'name'=>$post['name'],
            'sort'=>$post['sort'],
            'url'=>$post['url']
        ])->update();
        return json_encode([
            'status'=>1
        ]);
    }
    public function add()
    {

        $post = Request::post();
        $wm = new WechatMenu();
        $wm->data([
            'pid'=>$post['pid'],
            'name'=>$post['name'],
            'sort'=>$post['sort'],
            'url'=>$post['url']
        ])->insert();
        return json_encode([
            'status'=>1
        ]);

    }
    public function delete()
    {
        $wm = new WechatMenu();
        $status = $wm->where(['id'=>Request::get('id')])->delete();
        return json_encode([
            'status'=>1
        ]);
    }


}