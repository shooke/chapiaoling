<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-3-28
 * Time: 上午10:58
 */

namespace corephp\exception;

/**
 * 代码错误
 * @package corephp\exception
 */
class TypeErrorException extends \Exception
{
    protected $code = 500;
}