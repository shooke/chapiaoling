<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/30
 * Time: 0:20
 */

namespace api\ext;


class Cookie
{

    /**
     * cookie默认配置
     *
     * @var array
     */
    public static  $config = [
        //'name' => '',
        //'value' => '',
        //'prefix'=>'', //cookie前缀
        'expire' => 1200, //有效期
        'path' => '/', //有效路径
        'domain' => '',
        'secure' => false,
        'httponly' => false
    ];

    /**
     * 对象实例化时用于初始化配置
     * @param array $config
     */
    public function __construct($config = [])
    {
        self::$config = array_merge(self::$config,$config);
    }
    /**
     * 设置cookie的值
     *
     * @param string $name
     * @param string|array|object $value 所需至 会被json转化
     */
    public static function set($name, $value)
    {
        //对数据进行转换，如果是对象或数组这样做是必须的
        $value = json_encode($value);

        $config = self::$config;
        $expire = (int)$config['expire']+time();
        $path = $config['path'];
        $domain = $config['domain'];
        $secure = $config['secure'];
        $httponly = $config['httponly'];
        //设置cookie
        setcookie($name,$value,$expire,$path,$domain,$secure,$httponly);
    }
    /**
     * 获取cookie值
     *
     * 如果填写name则返回对应cookie，如果不填写则返回所有cookie
     *
     * @param string $name cookie元素下标
     * @param string $default 默认值，当获取不到值时使用
     * @param bool $object 是否返回object 默认false
     * @return Ambigous <unknown, string>|Ambigous <string, mixed>
     */
    public static function get($name = null, $default = null, $object=false)
    {
        //定义方法中用到的变量
        $output = null;
        //处理$_COOKIE数据
        if(!empty($_COOKIE)){
            foreach ($_COOKIE as $key=>$val){
                $output[$key] = json_decode($val,$object);
            }
        }

        //返回数据
        if (is_null($name)){
            return empty($output) ? $default : $_COOKIE;
        }else {
            //返回数据
            return isset($output[$name]) ? $output[$name] : $default;
        }
    }
    /**
     * 删除cookie
     *
     * 如果填写name则删除对应cookie，如果不填写则删除所有cookie
     *
     * @param  string $key      Cookie name
     * @param  array  $settings Optional cookie settings
     */
    public static function remove($name = null)
    {
        //设置日期过期
        self::$config['expire']=-100;
        if (is_null($name)){
            //删除所有cookie
            if($_COOKIE){
                foreach ($_COOKIE as $key=>$val){
                    self::set($key, '');
                }
            }
        }else {
            //删除对应cookie
            self::set($name, '');
        }
    }
}