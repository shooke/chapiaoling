<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-16
 * Time: 下午3:55
 */

namespace corephp\db\query;

/**
 * 查询解析接口
 * 任何解析都需要实现一下方法
 * @package corephp\db\query
 */
interface ParseQueryInterface
{
    public function insert();

    public function replace();

    public function update();

    public function delete();

    public function one();

    public function all();

    public function count($field = '*');

    public function max($field);

    public function min($field);

    public function avg($field);

    public function sum($field);
}