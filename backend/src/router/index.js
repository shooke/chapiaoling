import Vue from 'vue'
import Router from 'vue-router'
//网站设置
import SiteSetting from '@/components/site/setting'
//食品管理
import FoodAdd from '@/components/food/add'
import FoodEdit from '@/components/food/edit'
import FoodList from '@/components/food/list'
//食品分类
import SpeciesList from '@/components/species/list'
//用户密码
import UserPassword from '@/components/user/password'
//微信配置
import WechatBind from '@/components/wechat/bind'
import WechatQrcode from '@/components/wechat/qrcode'
//微信自定义菜单
import WechatMenuAdd from '@/components/wechat-menu/add'
import WechatMenuEdit from '@/components/wechat-menu/edit'
import WechatMenuList from '@/components/wechat-menu/list'
//文章管理
import ArticleAdd from '@/components/article/add'
import ArticleEdit from '@/components/article/edit'
import ArticleList from '@/components/article/list'

import CreateActive from '@/components/CreateActive'

Vue.use(Router)

export default new Router({
  routes: [

    {
      path: '/',
      component: FoodList
    },  
    //网站设置
    {
      path: '/site/setting',
      component: SiteSetting
    },
    //食品管理
    {
      path: '/food/add',
      component: FoodAdd
    },
    {
      path: '/food/edit/:id',
      component: FoodEdit
    },
    {
      path: '/food/list/:page',
      component: FoodList
    },

    {
      path: '/species/list',
      component: SpeciesList
    },
    //用户密码
    {
      path: '/user/password',
      component: UserPassword
    },
    //微信配置
    {
      path: '/wechat/bind',
      component: WechatBind
    },
    {
      path: '/wechat/qrcode',
      component: WechatQrcode
    },
    //微信自定义菜单
    {
      path: '/wechat-menu/add',
      component: WechatMenuAdd
    },
    {
      path: '/wechat-menu/edit/:id',
      component: WechatMenuEdit
    },
    {
      path: '/wechat-menu/list',
      component: WechatMenuList
    },
    //文章管理
    {
      path: '/article/add',
      component: ArticleAdd
    },
    {
      path: '/article/edit/:id',
      component: ArticleEdit
    },
    {
      path: '/article/list/:page',
      component: ArticleList
    }
    
  ]
})
