import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Jinshen from '@/components/Jinshen'
import Shaoliang from '@/components/Shaoliang'
import Fangxin from '@/components/Fangxin'
import ViewArticle from '@/components/ViewArticle'
import Food from '@/components/Food'
import Search from '@/components/Search'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Home,
      children: [
        {
          path: '',
          component: Jinshen
        },
        {
          path: 'jinshen',
          component: Jinshen
        },
        {
          path: 'shaoliang',
          component: Shaoliang
        },
        {
          path: 'fangxin',
          component: Fangxin
        }
      ]
    },
    {
      path:'/article/:id',
      component:ViewArticle
    },
    {
      path:'/food/:id',
      component:Food
    },
    {
      path:'/search',
      component:Search
    }
  ]
})
