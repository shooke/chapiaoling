# corephp2
##目录说明
- base 基础公用文件
	- autoload 自动载入
- controller 控制器
- exception 异常处理
- http 处理请求
- coredb 数据库处理
    - db 数据库链接
    - query 查询构造器
    - ar 表映射关系,字段映射
- route 路由处理
- view 视图处理
- validator 验证处理
- web web开发请求处理等

##功能说明
- 自动载入
- 异常处理
- 对象构造
- 路由处理
- 数据库处理
- 视图
- 控制器
- 验证器

##模型
- 可写
- 只读
##waitting
控制器方法反向注入，request

