<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-19
 * Time: 上午9:00
 */

namespace corephp\exception;

/**
 * 服务器找不到请求的网页
 * @package corephp\exception
 */
class NotFoundException extends \Exception
{
    protected $code = 404;
}