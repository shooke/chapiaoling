<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-16
 * Time: 下午3:49
 */

namespace corephp\route;


interface RouteInterface
{
    /**
     * 解析路由
     * @return array 返回['class'=>$className, 'action'=>$action, 'arguments'=>$call['arguments']
     * ]
     */
    public function parse();
    /**
     * 生成url
     * @param $ctrlAction
     * @param array $param
     * @param string $anchor
     * @return string
     */
    public function createUrl($ctrlAction, $param = [], $anchor = '');

}