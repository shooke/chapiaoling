<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/21
 * Time: 22:15
 */

namespace api\controllers;


use api\ext\UploadFile;
use api\models\Food;
use api\models\FoodSpecies;
use api\models\Wechat;
use corephp\web\Request;

class FoodController extends Base
{
    public function add()
    {
        $model = new Food();
        $model->data($_POST)->insert();//
        return json_encode([
            'status'=>1
        ]);
    }
    public function detail()
    {
        $id = Request::get('id');
        $model = new Food();
        $food = $model->where(['id'=>$id])->get();
        $species = (new FoodSpecies())->where(['id'=>$food['species_id']])->get();
        $food['species'] = $species['name'];
        $wechat = (new Wechat())->where(['id'=>1])->get();
        $food['qrcode'] = $wechat['qrcode'];
        return json_encode($food);
    }
    public function edit()
    {
        $id = Request::post('id');
        $model = new Food();
        $data = Request::post();
        unset($data['species']);
        unset($data['qrcode']);
        $model->data($data)->where(['id'=>$id])->update();//
        return json_encode([
            'status'=>1
        ]);
    }
    public function foods()
    {
        $speciesId  = (int)Request::get('species_id');
        $page = (int)Request::get('page',1);
        $page = $page >0 ? $page : 1;
        $limit = 10;
        $start = ($page-1)*$limit;
        $model = new Food();
        $model->where(['deleted'=>0]);
        if($speciesId){
            $model->andWhere(['species_id'=>$speciesId]);
        }
        $count = $model->count();
        if($count){
            $foods = $model->orderBy(['top'=>'DESC','id'=>'DESC'])->limit([$start,$limit])->select();
            $foodSpecies = (new FoodSpecies())->select();
            foreach ($foodSpecies as $val){
                $species[$val['id']] = $val['name'];
            }
            foreach ($foods as $key=>$val){
                $val['species'] = $species[$val['species_id']];
                $foods[$key] = $val;
            }
        }else{
            $foods=[];
        }

        return json_encode([
            'count'=>$count,
            'list'=>$foods
        ]);
    }
    public function delete()
    {
        $id = Request::get('id');
        (new Food())->where(['id'=>$id])->data(['deleted'=>1])->update();
        return json_encode([
            'status'=>1
        ]);
    }
    public function search()
    {
        $keyword = Request::get('keyword');
        $model = new Food();
        $result = $model->filterWhere([
            'name[~]'=>$keyword
        ])->select();
        return json_encode($result);
    }
    public function upload()
    {
        $upload = new UploadFile();// 实例化上传类
        $upload->maxSize  = 3145728 ;// 设置附件上传大小
        $upload->allowExts  = array('jpg', 'png', 'jpeg');// 设置附件上传类型
        $upload->savePath =  'upload/thumb/';// 设置附件上传目录
        if(!$upload->upload()) {// 上传错误提示错误信息
            $msg = $upload->getErrorMsg();
            $result['status']=0;
            $result['msg'] = $msg;
        }else{// 上传成功
            $imginfo=$upload->getUploadFileInfo();
            $result['status']=1;
            $result['msg'] = $imginfo[0];

        }
        return json_encode($result);
    }
}