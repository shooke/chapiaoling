<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-19
 * Time: 上午10:00
 */

namespace corephp\exception;

/**
 * 服务器拒绝请求
 * 用于创建文件错误等场景
 * @package corephp\exception
 */
class FileDeniedException extends \Exception
{
    protected $code = 403;
}