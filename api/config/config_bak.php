<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/10
 * Time: 0:26
 */
return [
    'debug'=>true,
    'date_timezone'=>'Asia/Shanghai',
    'controllerNamespace'=>'\api\controllers',
    'defaultRoute'=>'site/index',
    'autoload_path'=>[],
    'packages'=>[
        //数据库
        'db'=>[
            'class'=>\corephp\db\connect\Medoo::class,
            'param' => [

                [
                    // required
                    'database_type' => 'mysql',
                    'database_name' => 'chapiaoling',
                    'server'        => 'localhost',
                    'username'      => 'root',
                    'password'      => '',

                    // [optional]
                    'charset'       => 'utf8',
                    'port'          => 3306,

                    // [optional] Table prefix
                    'prefix'        => 'cp_',

                    // [optional] Enable logging (Logging is disabled by default for better performance)
                    'logging'       => true,


                    // [optional] driver_option for connection, read more from http://www.php.net/manual/en/pdo.setattribute.php
                    'option'        => [
                        \PDO::ATTR_CASE => \PDO::CASE_NATURAL
                    ],

                    // [optional] Medoo will execute those commands after connected to the database for initialization
                    'command'       => [
                        'SET SQL_MODE=ANSI_QUOTES'
                    ]
                ]
            ]
        ]
    ],
];