<?php
/**
 * Created by PhpStorm.
 * User: shooke
 * Date: 17-5-2
 * Time: 下午4:34
 */

namespace corephp\db\query;


class Query
{
    protected $field='*';
    protected $table;
    protected $where;
    protected $data;
    /**
     *  [
     * // Here is the table relativity argument that tells the relativity between the table you want to join.
     *
     * // The row author_id from table post is equal the row user_id from table account
     * "[>]account" => ["author_id" => "user_id"],
     *
     * // The row user_id from table post is equal the row user_id from table album.
     * // This is a shortcut to declare the relativity if the row name are the same in both table.
     * "[>]album" => "user_id",
     *
     * // [post.user_id is equal photo.user_id and post.avatar_id is equal photo.avatar_id]
     * // Like above, there are two row or more are the same in both table.
     * "[>]photo" => ["user_id", "avatar_id"],
     *
     * // If you want to join the same table with different value,
     * // you have to assign the table with alias.
     * "[>]account (replyer)" => ["replyer_id" => "user_id"],
     *
     * // You can refer the previous joined table by adding the table name before the column.
     * "[>]account" => ["author_id" => "user_id"],
     * "[>]album" => ["account.user_id" => "user_id"],
     *
     * // Multiple condition
     * "[>]account" => [
     * "author_id" => "user_id",
     * "album.user_id" => "user_id"
     * ]
     * ]
     * @var array
     */
    protected $join;
    protected $groupBy;
    protected $orderBy;
    protected $limit = [];
    protected $params = [];
    protected $uniqkey = 0;

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function field($field)
    {
        $this->field = $field;
        return $this;
    }

    public function data($data)
    {
        $this->data = $data;
        return $this;
    }

    public function groupBy($groupBy)
    {
        $this->groupBy = $groupBy;
        return $this;
    }

    public function orderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }


    /////////////////////////////////////////////////////////////////////
    //                            sql聚合函数处理                        //
    /////////////////////////////////////////////////////////////////////

//    public function count($field = '*')
//    {
//        $this->select = "count({$field})";
//        return $this;
//    }
//
//    public function max($field)
//    {
//        $this->select = "max({$field})";
//        return $this;
//    }
//
//    public function min($field)
//    {
//        $this->select = "min({$field})";
//        return $this;
//    }
//
//    public function avg($field)
//    {
//        $this->select = "avg({$field})";
//        return $this;
//    }
//
//    public function sum($field)
//    {
//        $this->select = "sum({$field})";
//        return $this;
//    }

    /////////////////////////////////////////////////////////////////////
    //                            where处理                             //
    /////////////////////////////////////////////////////////////////////

    /**
     * 原生查询
     * @param $condition
     * @param $params
     * @return $this
     */
    public function whereRaw($condition, $params)
    {
        $this->where = $condition;
        $this->addParams($params);
        return $this;
    }

    /**
     * 查询
     * @param $condition
     * @return $this
     */
    public function where($condition)
    {
        $this->where = $condition;
        return $this;
    }

    /**
     * and查询
     * @param $condition
     * @return $this
     */
    public function andWhere($condition)
    {
        $key = 'and' . $this->uniqkey();
        $this->where = [
            $key => array_merge($this->where,$condition)

        ];
        return $this;
    }

    /**
     * or查询
     * @param $condition
     * @return $this
     */
    public function orWhere($condition)
    {
        $key = 'or' . $this->uniqkey();
        $this->where = [
            $key => array_merge($this->where,$condition)
        ];
        return $this;
    }

    /**
     * 过滤空条件
     * @param $condition
     * @return $this
     */
    public function filterWhere($condition)
    {
        $this->where = $this->filter($condition);
        return $this;
    }

    /**
     * 过滤空条件
     * @param $condition
     * @return $this
     */
    public function andFilterWhere($condition)
    {
        $this->andWhere($this->filter($condition));
        return $this;
    }

    /**
     * 过滤空条件
     * @param $condition
     * @return $this
     */
    public function orFilterWhere($condition)
    {
        $this->orWhere($this->filter($condition));
        return $this;
    }


    /////////////////////////////////////////////////////////////////////
    //                            join处理                              //
    /////////////////////////////////////////////////////////////////////

    /**
     * inner join处理
     * $condition 可以是数组，也可以是字符串
     * // [>] == LEFT JOIN
     * // [<] == RIGH JOIN
     * // [<>] == FULL JOIN
     * // [><] == INNER JOIN
     * [
     * // Here is the table relativity argument that tells the relativity between the table you want to join.
     *
     * // The row author_id from table post is equal the row user_id from table account
     * "[>]account" => ["author_id" => "user_id"],
     *
     * // The row user_id from table post is equal the row user_id from table album.
     * // This is a shortcut to declare the relativity if the row name are the same in both table.
     * "[>]album" => "user_id",
     *
     * // [post.user_id is equal photo.user_id and post.avatar_id is equal photo.avatar_id]
     * // Like above, there are two row or more are the same in both table.
     * "[>]photo" => ["user_id", "avatar_id"],
     *
     * // If you want to join the same table with different value,
     * // you have to assign the table with alias.
     * "[>]account (replyer)" => ["replyer_id" => "user_id"],
     *
     * // You can refer the previous joined table by adding the table name before the column.
     * "[>]account" => ["author_id" => "user_id"],
     * "[>]album" => ["account.user_id" => "user_id"],
     *
     * // Multiple condition
     * "[>]account" => [
     * "author_id" => "user_id",
     * "album.user_id" => "user_id"
     * ]
     * ]
     * @param $table
     * @param $condition
     * @return $this
     */
    public function innerJoin($table, $condition)
    {
        $this->join["[><]{$table}"] = $condition;
        return $this;
    }

    public function fullJoin($table, $condition)
    {
        $this->join["[<>]{$table}"] = $condition;
        return $this;
    }

    public function leftJoin($table, $condition)
    {
        $this->join["[>]{$table}"] = $condition;
        return $this;
    }

    public function rightJoin($table, $condition)
    {
        $this->join["[<]{$table}"] = $condition;
        return $this;
    }


    /**
     * 进行过滤处理
     * @param $condition
     * @return mixed
     */
    protected function filter($condition)
    {
        if (is_array($condition)) {
            foreach ($condition as $name => $value) {
                if ($this->isEmpty($value)) {
                    unset($condition[$name]);
                }
            }
        }
        return $condition;
    }

    /**
     * 处理参数
     * @param $params
     */
    protected function addParams($params)
    {
        foreach ($params as $key => $value) {
            $this->params[$key] = $value;
        }
    }

    /**
     * 空字符串 空数组 null 全空格 都当做空处理
     * @param $value
     * @return bool
     */
    protected function isEmpty($value)
    {
        return $value === '' || $value === [] || $value === null || is_string($value) && trim($value) === '';
    }

    protected function uniqkey()
    {
        return ' #CorePhpQuery_' . $this->uniqkey++;
    }
}