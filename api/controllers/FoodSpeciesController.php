<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/21
 * Time: 21:39
 */

namespace api\controllers;


use api\models\FoodSpecies;
use corephp\web\Request;

class FoodSpeciesController extends Base
{
    public function species()
    {
        $wm = new FoodSpecies();
        $species = $wm->orderBy(['sort'=>'ASC'])->select();

        return json_encode($species);
    }
    public function edit()
    {
        $post = Request::post();
        $wm = new FoodSpecies();
        $wm->where(['id'=>$post['id']])->data([
            'name'=>$post['name'],
            'sort'=>$post['sort'],
        ])->update();
        return json_encode([
            'status'=>1
        ]);
    }
    public function add()
    {

        $post = Request::post();
        $wm = new FoodSpecies();
        $wm->data([
            'name'=>$post['name'],
            'sort'=>$post['sort'],
        ])->insert();
        return json_encode([
            'status'=>1
        ]);

    }
    public function delete()
    {
        $wm = new FoodSpecies();
        $status = $wm->where(['id'=>Request::get('id')])->delete();
        return json_encode([
            'status'=>1
        ]);
    }

}