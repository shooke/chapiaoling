<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/20
 * Time: 22:04
 */

namespace api\controllers;


use api\ext\Medoo;
use corephp\web\Controller;
use corephp\web\Request;

class Base extends Controller
{
    public function __construct()
    {
        header("Access-Control-Allow-Origin:*");
        header("Access-Control-Allow-Headers:x-requested-with,content-type");
        header("Access-Control-Request-Method:*");
    }


}