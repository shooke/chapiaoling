<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/5/9
 * Time: 23:09
 */

namespace corephp\base;


use corephp\exception\Exception;
use corephp\exception\TypeErrorException;
use corephp\log\File;
use corephp\route\Route;
use corephp\view\Template;

class Config
{
    private $_config=[
        'debug'=>false,
        'date_timezone'=>'Asia/Shanghai',
        'controllerNamespace'=>'\controllers',
        'defaultRoute'=>'site/index',
        'autoload_path'=>[],
        'packages'=>[],
    ];
    public function init($config)
    {
        $this->_config = array_merge($this->_config,$config);
    }
    //获取默认配置
    public function get( $name = null ) {
        if(is_null($name)){
            return $this->_config;
        }
        if(!is_string($name) && !is_int($name)){
            throw new TypeErrorException('参数类型错误');
        }
        if(isset($this->_config[$name])) {
            return $this->_config[$name];
        }
        return null;
    }

    //设置参数
    public function set($name, $value = array()) {
        return $this->_config[$name] = $value;
    }
}